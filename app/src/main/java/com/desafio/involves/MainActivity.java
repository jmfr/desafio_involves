package com.desafio.involves;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.desafio.involves.drawer.DrawerAdapter;
import com.desafio.involves.drawer.DrawerItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    protected RecyclerView recyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;

    private View v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();



        initializeAll();

    }

    private void initializeAll() {

        recyclerView = (RecyclerView) findViewById(R.id.cards_list);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        List<DrawerItem> drawerItens = new ArrayList<>();

        drawerItens.add(new DrawerItem("teste",13123));
        drawerItens.add(new DrawerItem("teste",13123));
        drawerItens.add(new DrawerItem("teste",13123));
        DrawerAdapter adapter = new DrawerAdapter(drawerItens);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            //if you want to use fragment you can use that on each option i just use a toast message
//            case R.id.navRl:
//                showMessage("Navigation Home");
//                closeDrawer();
//                break;
//            case R.id.navGuideRl:
//                showMessage("Navigation Guide");
//                closeDrawer();
//                break;
//            case R.id.navAboutUrlRl:
//                showMessage("Navigation About");
//                closeDrawer();
//
//                break;
            case R.id.navUrlRl:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"));
                startActivity(browserIntent);
                break;

        }
    }
    public void closeDrawer(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if (drawer.isDrawerOpen(GravityCompat.END)){
            drawer.closeDrawer(GravityCompat.END);
        }
    }

    public void showMessage(String message){
        Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
    }
}
