package com.desafio.involves.drawer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.desafio.involves.R;

import java.util.List;

public class DrawerAdapter extends RecyclerView.Adapter<LineHolder> {

    private final List<DrawerItem> mDrawerItem;

    public DrawerAdapter(List<DrawerItem> drawerItem) {
        mDrawerItem = drawerItem;
    }

    @Override
    public LineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LineHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drawer_line_view, parent, false));
    }

    @Override
    public void onBindViewHolder(LineHolder holder, int position) {
        DrawerItem drawerItem = mDrawerItem.get(position);
        holder.title.setText(drawerItem.getName());

    }

    @Override
    public int getItemCount() {
        return mDrawerItem != null ? mDrawerItem.size() : 0;
    }

}
class LineHolder extends RecyclerView.ViewHolder {

    public TextView title;

    public LineHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.tv_label);
    }
}