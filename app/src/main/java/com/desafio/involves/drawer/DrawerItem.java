package com.desafio.involves.drawer;

public class DrawerItem {

    private String name;

    private Integer drawable;

    public DrawerItem(String nome, Integer drawable){
        this.drawable = drawable;
        this.name = nome;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDrawable() {
        return drawable;
    }

    public void setDrawable(Integer drawable) {
        this.drawable = drawable;
    }
}
